/*
    SpriteTree is a utility program to place sprites relative to each other.
    Copyright (C) 2023-2024 Quentin Lambert

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
	*/

enum abstract EditAction(Int) to Int {
	var Load;
	var AddAnchor;
	var Up;
	var Down;
	var Left;
	var Right;
	var ScaleUp;
	var ScaleDown;
	var RotateClock;
	var RotateCounterClock;
	var Navigate;
	var Save;
}

enum abstract NavigateAction(Int) to Int {
	var Parent;
	var Child;
	var Next;
	var Previous;
	var Edit;
	var Save;
}

class Init extends dn.Process {
	public static var ME : Init;

	public var editController : dn.heaps.input.Controller<EditAction>;
	public var eca : dn.heaps.input.ControllerAccess<EditAction>;

	public var navController : dn.heaps.input.Controller<NavigateAction>;
	public var nca : dn.heaps.input.ControllerAccess<NavigateAction>;

	public function new() {
		super();
		ME = this;
		initControllers();

		delayer.addF( () -> { new Tool(); Tool.ME.load(); }, 1 );
	}

	function initControllers() {
		editController = dn.heaps.input.Controller.createFromAbstractEnum(EditAction);
		navController = dn.heaps.input.Controller.createFromAbstractEnum(NavigateAction);
		eca = editController.createAccess();
		nca = navController.createAccess();
		eca.lockCondition = () -> return destroyed;
		nca.lockCondition = () -> return destroyed;

		initEditControllerBindings();
		initNavControllerBindings();
	}

	public function initEditControllerBindings() {
		editController.removeBindings();

		editController.bindKeyboard(Load, hxd.Key.L);
		editController.bindKeyboard(AddAnchor, hxd.Key.A);
		editController.bindKeyboard(Up, hxd.Key.UP);
		editController.bindKeyboard(Down, hxd.Key.DOWN);
		editController.bindKeyboard(Left, hxd.Key.LEFT);
		editController.bindKeyboard(Right, hxd.Key.RIGHT);
		editController.bindKeyboard(ScaleUp, hxd.Key.PGUP);
		editController.bindKeyboard(ScaleDown, hxd.Key.PGDOWN);
		editController.bindKeyboard(RotateClock, hxd.Key.PGUP);
		editController.bindKeyboard(RotateCounterClock, hxd.Key.PGDOWN);
		editController.bindKeyboard(Navigate, hxd.Key.ESCAPE);
		editController.bindKeyboard(Save, hxd.Key.S);
	}
	
	public function initNavControllerBindings() {
		navController.removeBindings();

		navController.bindKeyboard(Parent, hxd.Key.UP);
		navController.bindKeyboard(Child, hxd.Key.DOWN);
		navController.bindKeyboard(Previous, hxd.Key.LEFT);
		navController.bindKeyboard(Next, hxd.Key.RIGHT);
		navController.bindKeyboard(Edit, hxd.Key.ESCAPE);
		navController.bindKeyboard(Save, hxd.Key.S);
	}
}
