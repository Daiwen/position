/*
    SpriteTree is a utility program to place sprites relative to each other.
    Copyright (C) 2023-2024 Quentin Lambert

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
	*/

import ui.Components;
import Init.EditAction;
import Init.NavigateAction;
import Tree;
import Current;
import Current.Utils as CUtils;
import spriteTree.SpriteData.Utils as SDUtils;
import spriteTree.SpriteData;

enum Mode {
	Navigate;
	Edit;
}

typedef Data = {
	var hook : h2d.Object;
	var object : h2d.Drawable;
	var anchors : Array<h2d.Drawable>;
}

class Tool extends dn.Process {
	public static var ME : Tool;

	private var eca(get, never) : dn.heaps.input.ControllerAccess<EditAction>; inline function get_eca() return Init.ME.eca;
	private var nca(get, never) : dn.heaps.input.ControllerAccess<NavigateAction>; inline function get_nca() return Init.ME.nca;
	private var uiRoot(get, never) : h2d.Object; inline function get_uiRoot() return Main.ME.uiRoot;

	private var mode : Mode;
	private var current : Current;
	private var tree : Tree<Data>;

	public var scroller : h2d.Object;
	private var camera : Camera;

	private var ignoreInput = false;

	private var sprites : Map<String, SpriteData>;
	private var allNodes : Map<String, Array<Node<Data>>>;

	public function new() {
		super();
		ME = this;

		mode = Edit;

		tree = new Tree<Data>();

		createRootInLayers(Main.ME.objectScene, 1);

		scroller = new h2d.Object(root);
		var g = new h2d.Graphics(scroller);
		g.beginFill(0xffffff, 0.);
		g.lineStyle(2, 0xffffff);
		var side = sideSize;
		g.drawRect(-side/2, -side/2, side, side);
		g.endFill();
		camera = new Camera();

		sprites = new Map();
		allNodes = new Map();
	}

	static var maxWidth = 100;
	private function getFileTiles() {
		var w = maxWidth;
		var h = 20;
		var baseTile = new h2d.SpriteBatch(h2d.Tile.fromColor(0x425e9e, w, h, 1));
		baseTile.add(new h2d.SpriteBatch.BatchElement(h2d.Tile.fromColor(0x425e9e, w, h, 1)));
		var hoverTile = new Array();
		hoverTile.push(new h2d.SpriteBatch.BatchElement(h2d.Tile.fromColor(0x425e9e, w, h, 1)));
		hoverTile.push(new h2d.SpriteBatch.BatchElement(h2d.Tile.fromColor(0xeeeeee, w, h, 0.3)));
		var activeTile = new h2d.SpriteBatch(h2d.Tile.fromColor(0x425e9e, w, h, 1));
		activeTile.add(new h2d.SpriteBatch.BatchElement(h2d.Tile.fromColor(0x425e9e, w, h, 1)));
		activeTile.add(new h2d.SpriteBatch.BatchElement(h2d.Tile.fromColor(0xeeeeee, w, h, 0.3)));
		return {
			width : w,
			height : h,
			hover : hoverTile,
			active : activeTile,
			base : baseTile,
		};
	}

	function readPixels(file:String): hxd.Pixels {
		var handle = sys.io.File.read(file, true);
		var d = new format.png.Reader(handle).read();
		var hdr = format.png.Tools.getHeader(d);
		var bytes = format.png.Tools.extract32(d);
		var width = hdr.width;
		var height = hdr.height;
		handle.close();

		return new hxd.Pixels(width, height, bytes, RGBA);
	}

	function addImage(root, n) {
		if (!sprites.exists(n)) {
			sprites.set(n, { name : n, position : new h2d.col.Point(0, 0), scale : 1., anchors : new Array() });
		}

		var hook = new h2d.Object(root);
		hook.name = root.name;

		var pixels = readPixels(n);
		var tile = h2d.Tile.fromPixels(pixels);

		var color = dn.RandomTools.color();
		var g = new h2d.Bitmap(tile, hook);
		g.colorAdd = h3d.Vector.fromColor(0x5b2c90);
		g.name = n;

		var data : Data = { hook : hook, object : g, anchors : new Array() };

		var p = if (current == null) null else current.node;
		var node = tree.addNode(p, data);

		var nodes = allNodes.get(n);
		if (null == nodes) {
			nodes = new Array();
			allNodes.set(n, nodes);
		}
		nodes.push(node);

		var current = { node : node, kind : Picture };
		setCurrent(current);
	}

	function openImage(root) {
		var container = new h2d.Flow(uiRoot);
		container.layout = Vertical;

		var files = sys.FileSystem.readDirectory(".");
		var r = ~/.*\.png/;
		var imgs = files.filter(r.match);

		for (n in imgs) {
			var settings = getFileTiles();
			var b = new ui.ButtonComp(settings, container);
			b.label = n;
			b.setVerticalMargin(5);
			b.onClick = () -> { container.remove(); addImage(root, n); };
		}

		recenter(container);
		var p = root.localToGlobal();
		container.x += p.x;
		container.y += p.y;
	}

	private static final sideSize = 10;
	function addAnchor(current : Node<Data>, name : String) {
		var color = 0x90f963;
		var g = new h2d.Graphics(current.data.hook);
		g.name = name;
		g.beginFill(color, 0.);
		g.lineStyle(2, color);
		var side = sideSize;
		g.drawRect(-side/2, -side/2, side, side);
		g.endFill();
		g.color = new h3d.Vector4(1,0,0,1);
		current.data.anchors.push(g);

		var current = { node : current, kind : Anchor (current.data.anchors.length-1) };
		return current;
	}

	function createAnchor() {
		var container = new h2d.Flow(uiRoot);
		var settings = getFileTiles();
		var b = new ui.ButtonComp(settings, container);
		var textinput = new h2d.TextInput(hxd.res.DefaultFont.get(), b);
		b.onClick = () -> { textinput.focus(); };
		textinput.onFocus = (_) -> { this.ignoreInput = true; };
		textinput.onFocusLost = (_) -> {
			this.ignoreInput = false;
			container.remove();
			var name = textinput.text;
			var data = sprites.get(current.node.data.object.name);
			data.anchors.push({ name : name, point : new h2d.col.Point(0, 0), angle : 0 });
			var ncurrent = addAnchor(current.node, name);
			setCurrent(ncurrent);
		};

		recenter(container);
		var p = CUtils.getLocationObject(current).localToGlobal();
		container.x += p.x;
		container.y += p.y;

		textinput.focus();
	}

	private function updateNodeFromData(n : Node<Data>, data : SpriteData){
		n.data.object.x = data.position.x;
		n.data.object.y = data.position.y;
		n.data.object.scaleX = data.scale;
		n.data.object.scaleY = data.scale;

		for (i in 0...data.anchors.length) {
			if (i >= n.data.anchors.length) {
				addAnchor(n, data.anchors[i].name);
			}

			n.data.anchors[i].x = data.anchors[i].point.x;
			n.data.anchors[i].y = data.anchors[i].point.y;
			n.data.anchors[i].rotation = data.anchors[i].angle;
		}
	}

	override function update() {
		super.update();

		for (i in sprites.keyValueIterator()) {
			var ns = allNodes.get(i.key);

			if (null == ns) {
				continue;
			}

			for (n in ns) {
				updateNodeFromData(n, i.value);
			}
		}

		if (ignoreInput)
			return;

		switch (mode) {
			case Edit: edit();
			case Navigate: navigate();
		}
	}

	private function setCurrent(c : Current) {
		if (null != current) {
			CUtils.downlight(current);
		}

		CUtils.highlight(c);

		camera.trackTarget(CUtils.getLocationObject(c), false);

		current = c;
	}

	function navigate() {
		if ( nca.isPressed(Save)) {
			save();
		}

		if ( nca.isPressed(Edit) ) {
			mode = Edit;
		}

		if ( nca.isPressed(Parent) ) {
			switch current.kind {
				case Picture:
					var name = current.node.data.hook.name;
					var parent = tree.getParent(current.node);
					var i = 0;
					for ( a in parent.data.anchors) {
						if (a.name == name) {
							setCurrent({ node : parent, kind : Anchor (i) });
							break;
						}
						i++;
					}
				case Anchor (_):
					setCurrent({ node : current.node, kind : Picture });
			}
		}

		if ( nca.isPressed(Child) ) {
			switch current.kind {
				case Picture if (current.node.data.anchors.length > 0):
					setCurrent({ node : current.node, kind : Anchor (0) });
				case Picture:
				case Anchor (i):
					var name = current.node.data.anchors[i].name;
					var children = tree.getChildren(current.node);
					for ( c in children) {
						if (c.data.hook.name == name) {
							setCurrent({ node : c, kind : Picture });
							break;
						}
					}
			}
		}

		if ( nca.isPressed(Next) ) {
			switch current.kind {
				case Picture:
				case Anchor (i):
					var l = current.node.data.anchors.length;
					var index = (i + 1) % l;
					setCurrent({ node : current.node, kind : Anchor (index) });
			}
		}

		if ( nca.isPressed(Previous) ) {
			switch current.kind {
				case Picture:
				case Anchor (i):
					var l = current.node.data.anchors.length;
					var index = (i - 1 + l) % l;
					setCurrent({ node : current.node, kind : Anchor (index) });
			}
		}
	}

	function edit() {
		if ( null == current && eca.isPressed(Load) ) {
			openImage(scroller);
		}

		if ( null == current) {
			return;
		}

		if ( eca.isPressed(Save)) {
			save();
		}

		if ( eca.isDown(Up) ) {
			CUtils.moveUp(current, sprites);
		}

		if ( eca.isDown(Down) ) {
			CUtils.moveDown(current, sprites);
		}

		if ( eca.isDown(Left) ) {
			CUtils.moveLeft(current, sprites);
		}

		if ( eca.isDown(Right) ) {
			CUtils.moveRight(current, sprites);
		}

		switch (current.kind) {
			case Picture:
				if ( eca.isDown(ScaleUp) ) {
					CUtils.scaleUp(current, sprites);
				}

				if ( eca.isDown(ScaleDown) ) {
					CUtils.scaleDown(current, sprites);
				}

				if ( eca.isPressed(AddAnchor) ) {
					createAnchor();
				}

			case Anchor (i):
				if ( eca.isPressed(Load) ) {
					openImage(current.node.data.anchors[i]);
				}

				if ( eca.isDown(RotateClock) ) {
					CUtils.rotateClockwise(current, i, sprites);
				}

				if ( eca.isDown(RotateCounterClock)) {
					CUtils.rotateCounterClockwise(current, i, sprites);
				}
		}

		if ( eca.isPressed(Navigate) ) {
			mode = Navigate;
		}
	}

	static final fileName = "spriteTree";
	function save() {
		SDUtils.save(sprites, fileName);
	}

	public function load() {
		var empty = new Map();
		sprites = SDUtils.load(empty, fileName);
	}

	static private function recenter(o : h2d.Object) {
		var b = o.getBounds();
		o.x = -b.width/2;
		o.y = -b.height/2;
	}
}
