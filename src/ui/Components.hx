/*
    SpriteTree is a utility program to place sprites relative to each other.
    Copyright (C) 2023 Quentin Lambert

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
	*/

package ui;

typedef WidgetSettings = {
	var width : Int;
	var height : Int;
	var hover : Array<h2d.SpriteBatch.BatchElement>;
	var active : h2d.SpriteBatch;
	var base : h2d.SpriteBatch;
}

class ButtonComp extends h2d.Object {

	public var width(default, null) : Float;
	public var height(default, null) : Float;

	public var marginUp : Float;
	public var marginDown : Float;
	public var marginLeft : Float;
	public var marginRight : Float;

	var activeTile : h2d.SpriteBatch;
	var baseTile : h2d.SpriteBatch;
	var hoverTile : Array<h2d.SpriteBatch.BatchElement>;

	var background : h2d.Object;
	var currentBatch : h2d.SpriteBatch;
	var boundTile : h2d.Bitmap;
	var interactive : h2d.Interactive;

	var active = false;
	var hover = false;

	public var propagateEvents(get, set) : Bool;
	inline function get_propagateEvents() return interactive.propagateEvents;
	inline function set_propagateEvents(b : Bool) return interactive.propagateEvents = b;

	public var labelTxt : h2d.Text;
	public var label(get, set): String;
	function get_label() return labelTxt.text;
	function set_label(s) {
		labelTxt.text = s;
		updateText();
		return s;
	}

	dynamic public function getName() {
		return label;
	}

	public function new( t: WidgetSettings, ?parent ) {
		super(parent);

		activeTile = t.active;
		hoverTile = t.hover;
		baseTile = t.base;

		currentBatch = baseTile;

		background = new h2d.Object(this);
		background.addChild(baseTile);

		labelTxt = new h2d.Text(hxd.res.DefaultFont.get(), this);

		interactive = new h2d.Interactive(t.width, t.height, this);
		
		interactive.onClick = (_) -> click();
		interactive.onPush = (_) -> push();
		interactive.onRelease = (_) -> release();
		interactive.onOut = (_) -> out();
		interactive.onOver = (_) -> over();
		interactive.onKeyDown = (e) -> keyDown(e.keyCode);

		width = t.width;
		height = t.height;

		syncScale();

		marginUp = 0;
		marginDown = 0;
		marginLeft = 0;
		marginRight = 0;

		var bTile =
			h2d.Tile.fromColor(
					0xffffff,
					Std.int(width + marginLeft + marginRight),
					Std.int(height + marginUp + marginDown),
					0.);

		boundTile = new h2d.Bitmap(bTile, this);
	}

	public function click() {
		onClick();
	}

	public function push() {
		active = true;
		showActive();
		onPush();
	}

	public function release() {
		active = false;
		showBase();
	}

	public function out() {
		hover = false;
		hideHover();
		onOut();
	}

	public function over() {
		hover = true;
		showHover();
		onOver();
	}

	public function keyDown(k : Int) {
		onKeyDown(k);
	}

	public function unroll(_) {
		onUnroll();
	}

	public function logSelect() {
		click();
	}

	public function rollBack() {
	}

	private function syncScale() {
		var b = activeTile.getBounds();
		activeTile.scale(width/b.width);
		b = baseTile.getBounds();
		baseTile.scale(width/b.width);
	}

	public dynamic function onOver() {
	}

	public dynamic function onOut() {
	}

	public dynamic function onClick() {
	}

	public dynamic function onPush() {
	}

	public dynamic function onKeyDown(k : Int) {
	}

	public dynamic function onUnroll() {
	}

	public function focus() {
		interactive.focus();
	}

	public function blur() {
		interactive.blur();
	}

	public function showHover() {
		hideHover();
		for (e in hoverTile)
			currentBatch.add(e);
	}

	public function hideHover() {
		for (e in hoverTile)
			e.remove();
	}

	public function showActive() {
		background.removeChildren();
		background.addChild(activeTile);
		currentBatch = activeTile;

		if (hover)
			showHover();
	}

	public function showBase() {
		background.removeChildren();
		background.addChild(baseTile);
		currentBatch = baseTile;

		if (hover)
			showHover();
	}

	public function setVerticalMargin(m : Float) {
		marginUp = marginDown = m;
		updateBounds();
	}

	public function setHorizontalMargin(m : Float) {
		marginLeft = marginRight = m;
		updateBounds();
	}

	private function updateBounds() {
		var w = Std.int(width + marginLeft + marginRight);
		var h = Std.int(height + marginUp + marginDown);
		var bTile = h2d.Tile.fromColor(0xffffff, w, h, 0.);
		boundTile.tile = bTile;
		background.setPosition(marginLeft, marginUp);
		interactive.setPosition(marginLeft, marginUp);
		updateText();
	}

	private function updateText() {
		labelTxt.x = Std.int(width/2 - labelTxt.textWidth/2) + marginLeft;
		labelTxt.y = Std.int(height/2 - labelTxt.textHeight/2) + marginUp;
	}

	public function resize(w, h) {
		this.width = w;
		this.height = h;

		this.interactive.width = w;
		this.interactive.height = h;

		updateBounds();
		syncScale();
	}
}
