/*
    SpriteTree is a utility program to place sprites relative to each other.
    Copyright (C) 2023 Quentin Lambert

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
	*/

typedef Node<T> = {
	var index : Int;
	var data : T;
}

class Tree<T> {
	var nodes : Array<Node<T>>;
	var parent : Array<Int>;
	var children : Array<Array<Int>>;
	var index : Int;

	public function new() {
		nodes = new Array();
		parent = new Array();
		children = new Array();

		index = 0;
	}

	public function addNode(p : Null<Node<T>>, d : T) {
		var n = { data : d, index : index };
		nodes.push(n);
		children[index] = new Array();

		if (null != p) {
			var p = p.index;
			parent[index] = p;
			children[p].push(index);
		}

		index++;
		return n;
	}

	public function getChildren(n : Node<T>) : Array<Node<T>>{
		return children[n.index].map((i) -> nodes[i]);
	}

	public function getParent(n : Node<T>) : Node<T> {
		return nodes[parent[n.index]];
	}
}
