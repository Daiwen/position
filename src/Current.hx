/*
    SpriteTree is a utility program to place sprites relative to each other.
    Copyright (C) 2023 Quentin Lambert

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
	*/

import Tree;
import Tool.Data;
import spriteTree.SpriteData;

enum Kind {
	Picture;
	Anchor(i : Int);
}

typedef Current = {
	var node : Node<Data>;
	var kind : Kind;
}

class Utils {
	static public function getLocationObject(c : Current) {
		return
			switch c.kind {
				case Picture : c.node.data.hook.parent;
				case Anchor (i): c.node.data.anchors[i];
			};
	}

	static public function getLocationPoint(c : Current, data : SpriteData) {
		return
			switch c.kind {
				case Picture : data.position;
				case Anchor (i): data.anchors[i].point;
			};
	}

	static public function getObject(c : Current) {
		return
			switch c.kind {
				case Picture : c.node.data.object;
				case Anchor (i): c.node.data.anchors[i];
			};
	}

	static public function moveUp(c : Current, sprites : Map<String, SpriteData>) {
		var data = sprites.get(c.node.data.object.name);
		var o = getLocationPoint(c, data);
		o.y--;
	}

	static public function moveDown(c : Current, sprites : Map<String, SpriteData>) {
		var data = sprites.get(c.node.data.object.name);
		var o = getLocationPoint(c, data);
		o.y++;
	}

	static public function moveLeft(c : Current, sprites : Map<String, SpriteData>) {
		var data = sprites.get(c.node.data.object.name);
		var o = getLocationPoint(c, data);
		o.x--;
	}

	static public function moveRight(c : Current, sprites : Map<String, SpriteData>) {
		var data = sprites.get(c.node.data.object.name);
		var o = getLocationPoint(c, data);
		o.x++;
	}

	static public function scaleUp(c : Current, sprites : Map<String, SpriteData>) {
		var data = sprites.get(c.node.data.object.name);
		data.scale += 0.05;
	}

	static public function scaleDown(c : Current, sprites : Map<String, SpriteData>) {
		var data = sprites.get(c.node.data.object.name);
		data.scale -= 0.05;
		if (data.scale < 0) {
			data.scale = 0.;
		}
	}

	static public function rotateClockwise(c : Current, i : Int, sprites : Map<String, SpriteData>) {
		var data = sprites.get(c.node.data.object.name);
		data.anchors[i].angle += 0.05;
		data.anchors[i].angle %= 2 * Math.PI;
	}

	static public function rotateCounterClockwise(c : Current, i : Int, sprites : Map<String, SpriteData>) {
		var data = sprites.get(c.node.data.object.name);
		data.anchors[i].angle -= 0.05;

		if (data.anchors[i].angle < 0) {
			data.anchors[i].angle += 2 * Math.PI;
		}
	}

	static public function downlight(c : Current) {
			switch c.kind {
				case Picture :
					var color = h3d.Vector.fromColor(0x5b2c90);
					c.node.data.object.colorAdd = color;
				case Anchor (i):
					c.node.data.anchors[i].color = new h3d.Vector4(1,0,0,1);
			};
	}

	static public function highlight(c : Current) {
			switch c.kind {
				case Picture :
					var color = h3d.Vector.fromColor(0x90f963);
					c.node.data.object.colorAdd = color;
				case Anchor (i):
					c.node.data.anchors[i].color = new h3d.Vector4(1,1,1,1);
			};
	}
}
